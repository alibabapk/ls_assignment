#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <fcntl.h> 
#include <errno.h>
#include <grp.h>
#include <pwd.h>



extern int errno;
void do_ls(char*);
void show_stat_info(char*);
void permissions(char*);
void file_type(char*);
void time_convert(long);
void group_resolve(int);
void user_resolve(int);
//26
int main(int argc, char* argv[]){
   if (argc == 1){
        // printf("Directory listing of pwd:\n");
      do_ls(".");
   }
   else{
      int i = 0;
      while(++i < argc){
         // printf("Directory listing of %s:\n", argv[i] );
	 //do_ls(argv[i]);
	 //file_type(argv[i]);
	 //permissions(argv[i]);
	 //time_convert(argv[i]);
	 //show_stat_info(argv[i]);
      }
   }

return 0;
}
//checking permissions
void permissions(char *argv){
struct stat buf;
   if (lstat(argv, &buf)<0){
      //perror("Error in stat");
      exit(1);
   }
   int mode = buf.st_mode; 
   char str[10];
   strcpy(str, "---------");
//owner  permissions
   if((mode & 0000400) == 0000400) str[0] = 'r';
   if((mode & 0000200) == 0000200) str[1] = 'w';
   if((mode & 0000100) == 0000100) str[2] = 'x';
//60//group permissions
   if((mode & 0000040) == 0000040) str[3] = 'r';
   if((mode & 0000020) == 0000020) str[4] = 'w';
   if((mode & 0000010) == 0000010) str[5] = 'x';
//others  permissions
   if((mode & 0000004) == 0000004) str[6] = 'r';
   if((mode & 0000002) == 0000002) str[7] = 'w';
   if((mode & 0000001) == 0000001) str[8] = 'x';
//special  permissions
   if((mode & 0004000) == 0004000) str[2] = 's';
   if((mode & 0002000) == 0002000) str[5] = 's';
   if((mode & 0001000) == 0001000) str[8] = 't';
   printf("%s", str);
}
//checking file type
void file_type(char *argv){
struct stat buf;
   if (lstat(argv, &buf)<0){
      perror("Error in stat");
      exit(1);
   	}
   if ((buf.st_mode &  0170000) == 0010000) 
    //82            //printf("%s is a Named Pipe\n", argv[1]);
		printf("p");
   else if ((buf.st_mode &  0170000) == 0020000) 
                //printf("%s is a Character Special file\n", argv[1]);
		printf("c");
   else if ((buf.st_mode &  0170000) == 0040000) 
                //printf("%s is a Directory\n", argv[1]);
		printf("d");
   else if ((buf.st_mode &  0170000) == 0060000) 
                //printf("%s is a Block Special file\n", argv[1]);
		printf("b");
   else if ((buf.st_mode &  0170000) == 0100000) 
                //printf("%s is a Regular file\n", argv[1]);
		printf("-");
   else if ((buf.st_mode &  0170000) == 0120000) 
                //printf("%s is a Soft link\n", argv[1]);
		printf("l");
   else if ((buf.st_mode &  0170000) == 0140000) 
        //101        //printf("%s is a Socket\n", argv[1]);
		printf("s");
   //else
                //printf("Unknwon mode\n");
}
//listing files and folders of a directory
int directory_listing(char *dir){
DIR* dp = opendir(dir);
   chdir(dir);
   errno = 0;
   struct dirent* entry;
   while(1){
      entry = readdir(dp);
      if(entry == NULL && errno != 0){
         perror("readdir");
         return errno;
      }
      if(entry == NULL && errno == 0){
         printf("\nEnd of directory\n");
         return 0;
      }
      printf("%s  \t ",entry->d_name);
   }
   closedir(dp);
}
//line 127//time conversion from seconds to standard date and time
void time_convert(long secs)
{
//long secs = atoi(argv);
   //printf("Date for %ld secs since epoch: %s\n",secs, ctime(&secs));
     printf(ctime(&secs));
}

//general ls
void do_ls(char * dir)
{
   struct dirent * entry;
   DIR * dp = opendir(dir);
   if (dp == NULL){
      fprintf(stderr, "Cannot open directory:%s\n",dir);
      return;
   }
printf("\033[0;32m");
   errno = 0;
   while((entry = readdir(dp)) != NULL){
         if(entry == NULL && errno != 0){
  		perror("readdir failed");
		exit(1);
  	}else{
                if(entry->d_name[0] == '.')
                    continue;
        }     	printf("%s ",entry->d_name);
		//show_stat_info(entry->d_name);
   }
   closedir(dp);
}
//line 156//showing stats of a file from its inode stucture
void show_stat_info(char *fname)
{
   struct stat info;
   int rv = lstat(fname, &info);
  // if (rv == -1){
    //  perror("stat failed");
     // exit(1);
   //}
   //printf("mode: %o\n", info.st_mode);
        printf("\033[0;32m");

   printf(" %ld\t", info.st_nlink);
   //printf(" %d\t", info.st_uid);
    user_resolve(info.st_uid);
   //printf(" %d\t", info.st_gid);
	group_resolve(info.st_gid);
   printf(" %ld\t", info.st_size);
   time_convert(info.st_mtime);
if ((info.st_mode &  0170000) == 0040000)
printf("\033[7m %s \033[m\n");
else
   printf("%s\n", fname );

}

/*void show_stat_info2(char *fname)
{
   struct stat info;
   int rv = lstat(fname, &info);
   if (rv == -1){
      perror("stat failed");
      exit(1);
   }
	printf("\033[0;32m");
   printf("mode: %o\n", info.st_mode);
   printf("link count: %ld\n", info.st_nlink);
   printf("user: %d\n", info.st_uid);
   printf("group: %d\n", group_resolve(info.st_gid));
   printf("size: %ld\n", user_resolve(info.st_size));
   printf("modtime: %ld\n", info.st_mtime);
   printf("name: %s\n", fname );
	printf("\033[0m");
}*/
//line 192
void group_resolve(int gid)
{
//int gid = atoi(argv);
   struct group * grp = getgrgid(gid);

   //errno = 0;
  // if (grp == NULL){
    //  if (errno == 0)
          //printf("Record not found in /etc/group file.\n");
    //  else
          //perror("getgrgid failed");
  // }else
      //printf("Name of group is: %s \n", grp->gr_name);   
	printf("\t%s",grp->gr_name);
}

void user_resolve(int uid)
{
//int uid = atoi(argv);
  // errno = 0;
   struct passwd * pwd = getpwuid(uid);
  
  // if (pwd == NULL){
   //   if (errno == 0)
         //printf("Record not found in passwd file.\n");
    //  else
         //perror("getpwuid failed");
  // }
  // else
       //printf("Name of user is: %s\n",pwd->pw_name);   
	printf(pwd->pw_name);
}
